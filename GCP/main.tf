terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.40.0"
    }
  }
}

provider "google" {
  # Configuration options
    project = "sodiac"
    region = "us-east1"
    zone = "us-east1-b"
}


resource "google_compute_network" "sodiac_vpc" {
    name = "sodiac-vpc"  
    description = "VPC para la tarea de IaC"
    auto_create_subnetworks = false        
}

resource "google_compute_subnetwork" "sodiac_public_subnet" {
    name = "public-subnet"
    description = "Subred para recursos públicos"
    region = "us-east1"
    network = google_compute_network.sodiac_vpc.id
    ip_cidr_range = "10.0.0.0/24"

}


resource "google_compute_firewall" "sodiac_firewall" {
    name = "sodiac-firewall"
    network = google_compute_network.sodiac_vpc.id
    allow {
      protocol = "tcp"
      ports = [ "22", "80"]
    }
    

    source_ranges = [ "0.0.0.0/0" ]
    target_tags = [ "web" ]  
    
}


resource "google_compute_instance" "sodiac_vm" {
    name = "vm"
    machine_type = "e2-micro"
    zone = "us-east1-b"

    boot_disk {
        initialize_params {
          image = "debian-cloud/debian-11"
        }
      
    }
    
    network_interface {
      network = google_compute_network.sodiac_vpc.id
      subnetwork = google_compute_subnetwork.sodiac_public_subnet.id
      # para obtener una IP externa
      access_config {        
      }
    }

    

    metadata_startup_script = <<-EOF
    #! /bin/bash
    apt update
    apt install -y git docker.io
    sudo git clone https://github.com/Nexpeque/cicdworkshop
    cd cicdworkshop
    sed -i 's/npm build/npm run build/' Dockerfile
    docker build -t workshop:latest .
    docker run -d -p 80:80 workshop:latest
    EOF

    tags = [ "web" ]
    
  
}