terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.37.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = "packer"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "sod-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b"]
  private_subnets = []
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "web_server_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"

  name        = "web-server"
  description = "Security group for web-server with HTTP ports open within VPC"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}


module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 4.1.4"

  name = "sod-instance"

  ami                    = "ami-09d3b3274b6c5d4aa"
  instance_type          = "t2.micro"
  key_name               = "digitalmoney"
  monitoring             = true
  vpc_security_group_ids = [module.web_server_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]
  user_data_replace_on_change = true
  user_data              = <<EOF
          #!/bin/bash
          yum -y update
          yum install -y git
          amazon-linux-extras install docker -y
          service docker start
          systemctl enable docker
          usermod -a -G docker ec2-user
          git clone https://github.com/Nexpeque/cicdworkshop.git
          cd cicdworkshop
          sed -i 's/npm build/npm run build/' Dockerfile 
          docker build -t workshop:latest .
          docker run -d -p 80:80 workshop:latest
          echo done!!
          EOF
  
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}
